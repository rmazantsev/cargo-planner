import { useEffect, useState } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import './App.css';
import Footer from './components/footer';
import Header from './components/header';
import Main from './components/main';
import Sidebar from './components/sidebar';
import { IShipment } from './dto/IShipment';
import { BaseService } from './services/BaseServise';


function App() {

    const [shipments, setShipments] = useState([] as IShipment[]);
    const [searchText, setSearchText] = useState('1');
    const [filteredShipments, setfilteredShipments] = useState(shipments);
    const [newBoxesState, setnewBoxesState] = useState({} as IShipment);

    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem("localState") !== null) {
            const state = JSON.parse(localStorage.getItem("localState") ?? "{}") as IShipment[];
            setfilteredShipments(state);
            setShipments(state);
        } else {
            if (window.confirm("It seems there are no shipments yet. Do you want to load them from the server?")) {
                loadData();
            }
        }
    }, [])

    useEffect(() => {
        if (searchText !== "1") {
            setfilteredShipments(shipments.filter((item) =>
                item.name.toLowerCase().startsWith(searchText.toLowerCase())))
        } if ((searchText === "")) {
            setfilteredShipments(shipments);
        }

    }, [searchText])

    const loadData = async () => {
        let result = await BaseService.getAll<IShipment>("/");
        if (result.ok && result.data !== undefined) {
            setShipments(result.data);
            setfilteredShipments(result.data);
        } else {
            window.alert(`Can't load! Status code: ${result.statusCode}, ${result.message}`)
        }
    }

    const updateGlobalState = () => {
        let newShipments = [...shipments]
        for (const shipment of newShipments) {
            if (shipment.id === newBoxesState.id) {
                shipment.boxes = newBoxesState.boxes;
                break;
            }
        }
        localStorage.setItem("localState", JSON.stringify(newShipments))
    }

    const redirectAfterEnterHit = () => {            
            setfilteredShipments(shipments);
            navigate(`/${filteredShipments[0]?.name ?? "error"}`);     
    }

    return (
        <>
            <Header redirect={redirectAfterEnterHit} saveState={updateGlobalState} loadState={loadData} sendText={setSearchText} />
            <Sidebar shipments={filteredShipments} />
            <Routes>
                <Route path="/:companyName" element={
                    <Main saveState={setnewBoxesState} shipments={shipments} />}
                />
            </Routes>
            <Footer />
        </>
    );
}

export default App;
