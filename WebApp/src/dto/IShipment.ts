export interface IShipment{
    id: string;
    name: string;
    email: string;
    boxes: string | null;
}