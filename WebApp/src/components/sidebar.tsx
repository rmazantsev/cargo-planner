import { useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import { IShipment } from "../dto/IShipment";


const Sidebar = (props: { shipments: IShipment[] }) => {

    const sidebar = useRef<HTMLDivElement>(null);

    const toggleSidebar = () => {
        if (sidebar.current !== null) {
            if (sidebar.current.style.display === 'none' || sidebar.current.style.display === '') {
                sidebar.current.style.display = 'block';
            } else {
                sidebar.current!.style.display = 'none';
            }
        }
    }

    const hideSidebar = () => {
        sidebar.current!.style.display = 'none'
    }

    useEffect(() => {
        if (sidebar.current !== null && props.shipments.length < 100) {
            sidebar.current.style.display = 'block';
        } else { sidebar.current!.style.display = 'none'; }
    }, [props.shipments])

    return (
        <>

            <input type='button' onClick={toggleSidebar} className="sidebar__button_hidden" />
            <div ref={sidebar} className="sidebar">
                {props.shipments.map((shipment) => {
                    return (
                        <Link className="sidebar__element"
                            to={{ pathname: `/${shipment.name}` }}
                            onClick={hideSidebar}
                            key={shipment.id}>{shipment.name}
                        </Link>
                    )
                })}

            </div>
        </>

    )
}

export default Sidebar;