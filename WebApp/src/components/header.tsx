import { useRef } from "react";


const Header = (props = { sendText: (text: string) => {}, loadState: () => {}, saveState: () => {}, redirect: () => {} }) => {

    const search = useRef<HTMLInputElement>(null);

    const handleEnterPressed = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            if(search.current){
            search.current.value = "";
            search.current.blur();
            props.redirect();
            }
          }
    }

    return (
        <header className="header">
            <div className="header__inner-div">
                <span className="header__logo">Cargo Planner</span>
                <input className="header__input-search" ref={search} onChange={() => {
                    props.sendText(search.current?.value ?? '')
                }} onKeyDown={(e) => handleEnterPressed(e)} type="search" placeholder="Search" id="input" />
                <input className="header__button header__button-load" type="button" onClick={props.loadState} value="Load" id="loadbtn" />
                <input className="header__button header__button-save" type="button" onClick={props.saveState} value="Save" id="savebtn" />
            </div>
        </header>
    )
}

export default Header;