import { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { IShipment } from "../dto/IShipment";
import recalculateBays from "../utils/recalculateBays";

const Main = (props: { shipments: IShipment[], saveState: (state: IShipment) => void }) => {

    const boxinput = useRef<HTMLInputElement>(null);
    const boxoutput = useRef<HTMLDivElement>(null);
    const label = useRef<HTMLLabelElement>(null);
    const main = useRef<HTMLDivElement>(null);
    const error = useRef<HTMLDivElement>(null);

    const [boxes, setBoxes] = useState("");

    const { companyName } = useParams();

    const defaultProps: IShipment = {
        id: "",
        name: "",
        email: "",
        boxes: ""
    }

    const shipment: IShipment = props.shipments.find(shipment => shipment.name === companyName) ?? defaultProps;

    useEffect(() => {
        boxinput.current!.value = shipment.boxes ?? '';
        if (shipment.boxes !== null) {
            setBoxes(shipment.boxes);
        } else {
            boxoutput.current!.innerText = "Specify only numbers!";
        }
    }, [shipment])

    useEffect(() => {
        const boxessum: number | boolean = recalculateBays(boxes);
        if (boxessum === false) {
            boxoutput.current!.innerText = "Specify array of numbers!";
        } else {
            boxoutput.current!.innerText = `Number of required cargo bays:  ${boxessum}`;
            shipment.boxes = boxinput.current!.value; // save only when it's numbers
            props.saveState(shipment)   
        }
    }, [boxes])

    useEffect(() => {
        if (shipment.id === '') {
            main.current!.style.display = "none";
            error.current!.style.display = "block";
        } else {
            main.current!.style.display = "block";
            error.current!.style.display = "none";
        }
    })

    return (
        <>
            <div className="main-hidden-text" ref={error} id="error">Wrong company name!</div>
            <div className="main" ref={main} id="main">
                <div className="main__title">{shipment.name}</div>
                <a className="main__email" href={`mailto:${shipment.email}`} >{shipment.email}</a>
                <div className="main__text-output" ref={boxoutput} >Number of required cargo bays: </div>
                <label className="main__label-input" ref={label} htmlFor="boxinput">Cargo boxes</label>
                <input className="main__input" ref={boxinput}
                    onChange={() => {setBoxes(boxinput.current?.value ?? '');}}
                    type="text" id="boxinput" />
            </div>
        </>
    )
};

export default Main;