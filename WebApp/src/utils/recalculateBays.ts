

function recalculateBays(boxes: string) {
    if (boxes === "") {
        return false
    }
    const arr: Array<string> = boxes.split(",");
    let sum: number = 0;

    if (arr.slice(0, -1).includes('')) {
        return false;
    }
    for (const num of arr) {
        if (tryParseFloat(num) === -1) {
            return false;
        }
        sum += tryParseFloat(num);
    }
    return Math.ceil(sum / 10);
}


function tryParseFloat(any: any): number {
    if (any === '') {
        return 0;
    }
    if (!isNaN(any) && !any.startsWith('.')) {
        return parseFloat(any);
    }
    return -1;
}


export default recalculateBays;